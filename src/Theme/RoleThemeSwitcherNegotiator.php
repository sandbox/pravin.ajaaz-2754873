<?php

namespace Drupal\role_theme_switcher\Theme;

use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Routing\RouteMatchInterface;

class RoleThemeSwitcherNegotiator implements ThemeNegotiatorInterface {
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    // Use this theme on a certain route.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    $user_roles = \Drupal::currentUser()->getRoles();
    $user_role = end($user_roles);
    return \Drupal::config('role_theme_switcher.settings')->get($user_role);
  }

}
