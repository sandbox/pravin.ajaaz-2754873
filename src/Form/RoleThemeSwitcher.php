<?php

/**
 * @file
 * Contains \Drupal\role_theme_switcher\RoleThemeSwitcher
 */
namespace Drupal\role_theme_switcher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure RoleThemeSwitcher settings for this site.
 */
class RoleThemeSwitcher extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_theme_switcher';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'role_theme_switcher.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('role_theme_switcher.settings');
    $roles = user_roles();
    $theme_handler = \Drupal::service('theme_handler');
    $themes = $theme_handler->listInfo();
    foreach ($themes as $key => $value) {
      $theme_options[$key] = $value->getName();
    }
    foreach ($roles as $key => $value) {
      $roles_options[$key] = $value->label();
    }
    $form['roles'] = array(
      '#title' => t('Swtich themes per role'),
      '#type' => 'fieldset',
    );
    foreach ($roles_options as $key => $value) {
      $form['roles'][$key] = array(
        '#title' => $value,
        '#type' => 'select',
        '#options' => $theme_options,
        '#default_value' => $this->config('role_theme_switcher.settings')->get($key),
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $roles = user_roles();
    foreach ($roles as $key => $value) {
      $roles_options[$key] = $value->label();
    }
    foreach ($roles_options as $key => $value) {
      $this->config('role_theme_switcher.settings')
        ->set($key, $form_state->getValue($key))
        ->save();
    }
    parent::submitForm($form, $form_state);
  }
}
